const {describe} = require('mocha');
const {expect} = require('chai');

describe('Simple test suite (with chai):', function() {
    it('1 === 1 should be true', function() {
        expect(1).to.equal(1);
    });
});

describe('First test', () => {
  describe('42 == 42', () => {
	it('should be true', () => {
    expect(42).to.equal(42)
	})
  })
})

describe('Second test', () => {
  describe('0 == 0', () => {
	it('should be true', () => {
	  expect(0).to.equal(0)
	})
  })
})


